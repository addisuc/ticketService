package com.homework.ticketService.controller;

import org.apache.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.homework.ticketService.dao.entities.Seat;
import com.homework.ticketService.dao.repositories.SeatRepository;

@SpringBootApplication
@EnableJpaRepositories({"com.homework.ticketService.dao"})
@EntityScan({"com.homework.ticketService.dao"})
public class Application {
	private static final Logger logger = Logger.getLogger(Application.class);
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

	/**
	 * Loads the seat table with data when the application starts.
	 * @param repository
	 * @return
	 */
	@Bean
	public CommandLineRunner commandLineRunner(SeatRepository repository) {
		logger.info("################### Initializing Seats database...#######################");
		return args -> {
			for (int i = 1; i <= 100; i++) {
				Seat seat = new Seat(i, true);
				repository.save(seat);
			}
	    logger.info("###################### Initializing Seats database completed.#############");
		};
	}
}
