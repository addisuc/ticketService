package com.homework.ticketService.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.homework.ticketService.Impl.SeatHold;
import com.homework.ticketService.Impl.TicketServiceImpl;

@RestController
@RequestMapping("/")
public class ServiceController {
	TicketServiceImpl serviceImpl = new TicketServiceImpl();

	@RequestMapping(method = RequestMethod.POST, value = "findAndHoldSeats")
	public SeatHold findAndHoldSeats(int numSeats, String customerEmail) {
		// TODO: implement preconditions checks.And exception scenarios.
		return serviceImpl.findAndHoldSeats(numSeats, customerEmail);
	}

	@RequestMapping(method = RequestMethod.GET, value = "getNumberOfAvailableSeats")
	public int getNumberOfSeatsAvailable() {
		// TODO: implement preconditions checks and exceptions.
		return serviceImpl.numSeatsAvailable();
	}

	@RequestMapping(method = RequestMethod.PUT, value = "reserveSeats")
	public String reserveSeats(int seatHoldId, String customerEmail) {
		// TODO: implement preconditions.
		return serviceImpl.reserveSeats(seatHoldId, customerEmail);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "freeSeats")
	public String freeSeats(int seatHoldId, String customerEmail) {
		// TODO: implement preconditions.
		return serviceImpl.freeupSeat(seatHoldId, customerEmail);
	}
}
