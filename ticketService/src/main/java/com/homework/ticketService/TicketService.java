package com.homework.ticketService;

import com.homework.ticketService.Impl.SeatHold;

public interface TicketService {
	/**
	 * The number of seats in the venue that are neither held not reserved.
	 * 
	 * @return the number of tickets available in the venue
	 */
	int numSeatsAvailable();

	/**
	 * Find and hold the best available seats for a customer
	 * 
	 * @param numSeats
	 * @param customerEmail
	 * @return
	 */
	SeatHold findAndHoldSeats(int numSeats, String customerEmail);

	/**
	 * Commit seats held for a specific customer
	 * 
	 * @param seatHoldId
	 *            the seat hold identifier
	 * @param customerEmail
	 *            the email address of the customer to which the seat hold is
	 *            assigned.
	 * @return String
	 */
	String reserveSeats(int seatHoldId, String customerEmail);
}