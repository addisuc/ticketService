package com.homework.ticketService.Impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;

import com.homework.ticketService.TicketService;
import com.homework.ticketService.dao.entities.Reservation;
import com.homework.ticketService.dao.repositories.ReservationRepository;
import com.homework.ticketService.dao.repositories.SeatRepository;

/**
 * Implementation of the ticket service operations: 
 * 1. Find Available seats 
 * 2. Find and hold seats for a customer 
 * 3. commit reservations.
 * 4. Free seat for next reservation.
 * @author anibr
 *
 */
@PropertySource(value="configs/application-${spring.profiles.active}.properties")
public class TicketServiceImpl implements TicketService {
	private static final Logger logger = Logger.getLogger(TicketServiceImpl.class);

	@Resource
	private SeatRepository seatRepository;

	@Resource
	private ReservationRepository reservationRepository;
	
	@Value("${seatHoldDuration}")
	private int seatHoldDuration; //duration in seconds.

	/**
	 * Return the number of available seats.
	 */
	@Override
	public int numSeatsAvailable() {
		int numberOfAvailable = seatRepository.findNumberOfAvailableSeats();
		logger.info("NUM_SEATS_AVAILABLE: Successfully completed the 'numSeatsAvailable'");
		return numberOfAvailable;
	}

	/**
	 * Finds top N available seats and holds them on behalf of the customer.
	 */
	@Override
	public SeatHold findAndHoldSeats(int numSeats, String customerEmail) {
		SeatHold seatHold = new SeatHold();
		if (this.numSeatsAvailable() < numSeats) {
			seatHold.setMemo(ServiceErrors.SEATS_UNAVAILABLE.getMessage());
			logger.error("FIND_AND_HOLD_SEATS: Error occured while 'finding and holding seats'.");
			return seatHold;
		}

		List<Integer> availableSeats = seatRepository.findFixedNumberOfAvailableSeats(numSeats);
		seatHold.setSeatHoldIds(availableSeats);
		StringBuilder heldSeats = new StringBuilder();

		for (Integer seatHoldId : seatHold.getSeatHoldIds()) {
			Reservation reservation = new Reservation(customerEmail, seatHoldId, ReservationStatus.HOLD);
			reservationRepository.save(reservation);
			seatRepository.setSeatUnavailable(seatHoldId);
			heldSeats.append(", " + seatHoldId);
		}
		
		seatHold.setMemo(String.format(ServiceMessages.SEAT_SUCCSSFULLY_HOLD.getMessage(), heldSeats.toString()));
		logger.info("FIND_AND_HOLD_SEATS: Successfully completed the 'findAndHoldSeats'");
		startTimer(seatHold.getSeatHoldIds()); //start the count down for seat hold expiration.
		return seatHold;
	}

	/**
	 * Commits the seat reservation.
	 */
	@Override
	public String reserveSeats(int seatHoldId, String customerEmail) {
		//TODO: check if seat is actually held from the reservation. IT could have been removed from reservation due to hold expiration
		reservationRepository.reserveSeat(seatHoldId, ReservationStatus.RESERVED, customerEmail);
		String message = String.format(ServiceMessages.SEAT_SUCCSSFULLY_RESERVED.getMessage(), seatHoldId);
		logger.info("RESERVE_SEATS: Successfully completed the 'reserveSeats'");
		return message;
	}

	/**
	 * Makes seat available for new reservation. This happens due to expired hold or resetting entire reservation.
	 * @param seatHoldId
	 * @param customerEmail
	 * @return
	 */
	public String freeupSeat(int seatHoldId, String customerEmail) {
		reservationRepository.removeReservation(seatHoldId);
		seatRepository.makeSeatAvailable(seatHoldId);
		String message = String.format(ServiceMessages.SEAT_SUCCSSFULLY_FREED.getMessage(), seatHoldId);
		return message;
	}
	

	/**
	 * Count down for duration of {@link seatHoldDuration} to expire the seatHold.
	 * 
	 * @param seatHoldId
	 */
	private void startTimer(List<Integer> seatHoldIds) {
		long startTime = System.currentTimeMillis();
		long endTime = startTime + (seatHoldDuration * 1000);
		while (System.currentTimeMillis() < endTime) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				logger.error("START_TIMER: Error while running the seatHold timer.");
			}
		}
		// TODO: check if seat hold is not committed.Remove it from reservation
		// if not.
		for (Integer id : seatHoldIds) {
			seatRepository.makeSeatAvailable(id);
			reservationRepository.removeReservation(id);
		}
	}
}
