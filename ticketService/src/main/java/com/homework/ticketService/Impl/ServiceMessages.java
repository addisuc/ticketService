package com.homework.ticketService.Impl;

/**
 * Holds all the necessary user friendly messages.
 * @author anibr
 *
 */
public enum ServiceMessages {

	SEAT_SUCCSSFULLY_RESERVED("MESSAGE_001", "Seat is Successfully reserved.SeatId: %s"),
	SEAT_SUCCSSFULLY_FREED("MESSAGE_002", "Seat is Successfully freed.SeatId: %s"),
	SEAT_SUCCSSFULLY_HOLD("MESSAGE_003", "Seat is Successfully hold.SeatIds: %s");
	
	
	private final String message;
	private final String key;

	private ServiceMessages(String key, String message) {
		this.key = key;
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public String getKey() {
		return key;
	}
}
