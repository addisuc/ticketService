package com.homework.ticketService.Impl;

/**
 * Available statuses for reservation
 * @author anibr
 *
 */
public enum ReservationStatus {
	HOLD, RESERVED, AVAILABLE;
}
