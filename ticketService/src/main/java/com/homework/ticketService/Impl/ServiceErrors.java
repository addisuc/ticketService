package com.homework.ticketService.Impl;

/**
 * Holds all the service error messages.
 * @author anibr
 *
 */
public enum ServiceErrors {

	SEATS_UNAVAILABLE("ERROR_001", "The requested number of seats are unavailable.");

	private final String message;
	private final String key;

	private ServiceErrors(String key, String message) {
		this.key = key;
		this.message = message;
	}
	
	public String getMessage() {
		return message;
	}

	public String getKey() {
		return key;
	}
	
}
