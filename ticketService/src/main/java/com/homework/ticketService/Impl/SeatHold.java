package com.homework.ticketService.Impl;

import java.util.List;

/**
 * Holds the list of held seat ids and memo if any.
 * 
 * @author anibr
 *
 */
public class SeatHold {
	private List<Integer> seatHoldIds;
	private String memo;

	public List<Integer> getSeatHoldIds() {
		return seatHoldIds;
	}

	public void setSeatHoldIds(List<Integer> seatHoldIds) {
		this.seatHoldIds = seatHoldIds;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}
}
