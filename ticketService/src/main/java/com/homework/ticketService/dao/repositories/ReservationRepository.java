package com.homework.ticketService.dao.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.homework.ticketService.Impl.ReservationStatus;
import com.homework.ticketService.dao.entities.Reservation;

/**
 * Repository to interact with the underlying Reservation table via JPA mapping
 * 1. updates reservation table to turn 'hold' reservation to 'reserved' 
 * 2. removed reservation record incase of expired 'hold' status or when entire
 * venue restarts.
 * 
 * @author anibr
 *
 */
@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Long> {
	@Query("Update Reservation r set r.status = :status, r.customerEmail = :customerEmail where r.seatHoldId = :seatId")
	public void reserveSeat(@Param("seatId") int seatId, @Param("status") ReservationStatus status,
			@Param("customerEmail") String customerEmail);

	@Query("Delete from Reservation WHERE seatHoldId = :seatId")
	public void removeReservation(@Param("seatId") int seatId);

}
