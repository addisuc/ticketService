package com.homework.ticketService.dao.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.homework.ticketService.dao.entities.Seat;
/**
 * Repository to manipulate Seat table via JPA.
 * 1. finds all available seats
 * 2. find some number of available seats if exist
 * 3. turns seat to unavailable when it is on hold
 * 4. turns seat status to available when it is freed.\
 * 
 * @author anibr
 *
 */
@Repository
public interface SeatRepository extends JpaRepository<Seat, Long> {

	@Query("SELECT count(*) FROM Seat s where s.available = true")
	public int findNumberOfAvailableSeats();

	@Query("SELECT s.seatId FROM Seat s where s.available = true AND rownum <= :numSeats")
	public List<Integer> findFixedNumberOfAvailableSeats(@Param("numSeats") int numSeats);

	@Query("update Seat s set s.available = false where seatId= :seatId")
	public void setSeatUnavailable(@Param("seatId") int seatId);
	
	@Query("update Seat s set s.available = true where seatId= :seatId")
	public void makeSeatAvailable(@Param("seatId") int seatId);
}
