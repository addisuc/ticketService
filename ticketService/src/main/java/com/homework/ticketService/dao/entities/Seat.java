package com.homework.ticketService.dao.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
/**
 * Entity class to persist to Seat table
 * @author anibr
 *
 */
@Entity
public class Seat {
	
	@Id
	private int seatId;
	
	private boolean available;
	
	private String description;

	public Seat() {
	}

	public Seat(int seatId, boolean available) {

		this.seatId = seatId;
		this.available = available;
	}
	
	public int getSeatId() {
		return seatId;
	}

	public void setSeatId(int seatId) {
		this.seatId = seatId;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
