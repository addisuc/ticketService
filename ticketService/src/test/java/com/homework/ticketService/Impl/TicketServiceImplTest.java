package com.homework.ticketService.Impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.homework.ticketService.dao.entities.Reservation;
import com.homework.ticketService.dao.repositories.ReservationRepository;
import com.homework.ticketService.dao.repositories.SeatRepository;

public class TicketServiceImplTest {

	@InjectMocks
	private TicketServiceImpl ticketServiceImpl = new TicketServiceImpl();

	@Mock
	private SeatRepository seatRepository;

	@Mock
	private ReservationRepository reservationRepository;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testNumSeatsAvailable() {
		int expected = 100;
		when(seatRepository.findNumberOfAvailableSeats()).thenReturn(100);
		int result = ticketServiceImpl.numSeatsAvailable();
		assertEquals(expected, result);
	}

	@Test
	public void testFindAndHoldSeats() {
		int numSeatsRequested = 3;
		String customerEmail = "cus@email.com";
		TicketServiceImpl serviceSpy = Mockito.spy(ticketServiceImpl);
		when(serviceSpy.numSeatsAvailable()).thenReturn(4);
		List<Integer> availableSeats = new ArrayList<>();
		availableSeats.add(1);
		availableSeats.add(2);
		availableSeats.add(3);
		when(seatRepository.findFixedNumberOfAvailableSeats(numSeatsRequested)).thenReturn(availableSeats);
		when(reservationRepository.save(any(Reservation.class))).thenReturn(null); // do nothing
		doNothing().when(seatRepository).setSeatUnavailable(any(Integer.class)); // do nothing
		SeatHold seatHold = serviceSpy.findAndHoldSeats(numSeatsRequested, customerEmail);
		assertNotNull(seatHold);
		assertNotNull(seatHold.getSeatHoldIds());
		assertTrue(seatHold.getMemo() != null);
		assertTrue(seatHold.getSeatHoldIds().size() == 3);
		
	}

	/**
	 * Tests the scenario of requested number of seats unavailable.
	 */
	@Test
	public void testFindAndHoldSeats_SeatsUnAvailable() {
		int numSeatsRequested = 20;
		String customerEmail = "cus@email.com";
		TicketServiceImpl serviceSpy = Mockito.spy(ticketServiceImpl);
		when(serviceSpy.numSeatsAvailable()).thenReturn(10);
		SeatHold seatHold = serviceSpy.findAndHoldSeats(numSeatsRequested, customerEmail);
		assertNotNull(seatHold);
		assertTrue(seatHold.getSeatHoldIds() == null);
		assertTrue(seatHold.getMemo() != null);
	}

	@Test
	public void testReserveSeats() {
		int seatHoldId = 1;
		String customerEmail = "cust@xyz.com";
		doNothing().when(reservationRepository).reserveSeat(seatHoldId, ReservationStatus.RESERVED, customerEmail);
		String message = ticketServiceImpl.reserveSeats(seatHoldId, customerEmail);
		assertNotNull(message);
	}

	@Test
	public void testFreeupSeats() {
		int seatHoldId = 1;
		String customerEmail = "cust@xyz.com";
		doNothing().when(reservationRepository).removeReservation(seatHoldId);
		doNothing().when(seatRepository).makeSeatAvailable(seatHoldId);
		String message = ticketServiceImpl.freeupSeat(seatHoldId, customerEmail);
		assertNotNull(message);
	}
}
